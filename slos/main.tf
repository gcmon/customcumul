/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

terraform {
  backend "gcs" {
    bucket = "brunore-tfstate-orgops"
    prefix = "gmonslos-dev"
  }
}

data "google_project" "project" {
  project_id = var.project_id
}

resource "google_monitoring_custom_service" "app" {
  project      = var.project_id
  service_id   = var.app_name
  display_name = var.app_name
}

resource "google_pubsub_topic" "alerting" {
  project = var.project_id
  name    = var.alerting_topic_name
  message_storage_policy {
    allowed_persistence_regions = var.pubsub_allowed_regions
  }
}

resource "google_pubsub_topic_iam_member" "alerting_publisher" {
  project = google_pubsub_topic.alerting.project
  topic   = google_pubsub_topic.alerting.name
  role    = "roles/pubsub.publisher"
  member  = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-monitoring-notification.iam.gserviceaccount.com"
}

resource "google_monitoring_notification_channel" "alerting2pubsub" {
  project      = var.project_id
  display_name = "Alerting to Pub/sub"
  type         = "pubsub"
  labels = {
    "topic" = google_pubsub_topic.alerting.id
  }
}

resource "google_monitoring_slo" "slo_coverage" {
  for_each            = toset(var.slo_coverage.table_list)
  project             = var.project_id
  service             = google_monitoring_custom_service.app.service_id
  slo_id              = "${var.app_name}-${each.key}-coverage"
  display_name        = "${var.app_name} ${each.key} coverage: ${tostring(var.slo_coverage.goal * 100)}% of intervals should be processed with a score >=5 over the last ${var.slo_coverage.rolling_period_days} days"
  goal                = var.slo_coverage.goal
  rolling_period_days = var.slo_coverage.rolling_period_days
  request_based_sli {
    good_total_ratio {
      good_service_filter  = "metric.type=\"custom.googleapis.com/dataproducts/interval/count\" resource.type=\"global\" resource.label.project_id=\"${var.project_id}\" metric.labels.data_product_name=\"${var.app_name}\" metric.labels.table_name=\"${each.key}\" metric.labels.interval_duration_sec=\"${var.interval_duration_sec}\" metric.labels.good_points=one_of(\"5\",\"8\",\"9\")"
      total_service_filter = "metric.type=\"custom.googleapis.com/dataproducts/interval/count\" resource.type=\"global\" resource.label.project_id=\"${var.project_id}\" metric.labels.data_product_name=\"${var.app_name}\" metric.labels.table_name=\"${each.key}\" metric.labels.interval_duration_sec=\"${var.interval_duration_sec}\""
    }
  }
}

resource "google_monitoring_alert_policy" "slo_coverage_slow_burn" {
  for_each     = toset(var.slo_coverage.table_list)
  project      = var.project_id
  display_name = "${var.app_name} ${each.key} coverage burn rate last ${var.slo_coverage.alerting_slow_burn_loopback_period} > ${var.slo_coverage.alerting_slow_burn_threshold}"
  combiner     = "OR"
  conditions {
    display_name = "${var.app_name} ${each.key} coverage burn rate last ${var.slo_coverage.alerting_slow_burn_loopback_period} > ${var.slo_coverage.alerting_slow_burn_threshold}"
    condition_threshold {
      filter          = "select_slo_burn_rate(\"${google_monitoring_slo.slo_coverage[each.key].id}\", \"${var.slo_coverage.alerting_slow_burn_loopback_period}\")"
      duration        = "0s"
      comparison      = "COMPARISON_GT"
      threshold_value = var.slo_coverage.alerting_slow_burn_threshold
      trigger {
        count = 1
      }
    }
  }
  notification_channels = [
    "${google_monitoring_notification_channel.alerting2pubsub.name}"
  ]
}

resource "google_monitoring_alert_policy" "slo_coverage_fast_burn" {
  for_each     = toset(var.slo_coverage.table_list)
  project      = var.project_id
  display_name = "${var.app_name} ${each.key} coverage burn rate last ${var.slo_coverage.alerting_fast_burn_loopback_period} > ${var.slo_coverage.alerting_fast_burn_threshold}"
  combiner     = "OR"
  conditions {
    display_name = "${var.app_name} ${each.key} coverage burn rate last ${var.slo_coverage.alerting_fast_burn_loopback_period} > ${var.slo_coverage.alerting_fast_burn_threshold}"
    condition_threshold {
      filter          = "select_slo_burn_rate(\"${google_monitoring_slo.slo_coverage[each.key].id}\", \"${var.slo_coverage.alerting_fast_burn_loopback_period}\")"
      duration        = "0s"
      comparison      = "COMPARISON_GT"
      threshold_value = var.slo_coverage.alerting_fast_burn_threshold
      trigger {
        count = 1
      }
    }
  }
  notification_channels = [
    "${google_monitoring_notification_channel.alerting2pubsub.name}"
  ]
}

resource "google_monitoring_dashboard" "slo_coverage_dashboard" {
  for_each       = toset(var.slo_coverage.table_list)
  project        = var.project_id
  dashboard_json = <<EOF
{
    "displayName": "slo_1_${var.app_name}_${each.key}_coverage",
    "mosaicLayout": {
        "columns": 12,
        "tiles": [
            {
                "height": 20,
                "width": 12,
                "widget": {
                    "collapsibleGroup": {},
                    "title": "${tostring(var.slo_coverage.goal * 100)}% of intervals should be processed with a score >=5 over the last ${var.slo_coverage.rolling_period_days} days"
                }
            },
            {
                "height": 4,
                "width": 12,
                "widget": {
                    "title": "How much of errbdg, as a fraction from - infinity to 1, remains at this time?",
                    "xyChart": {
                        "chartOptions": {
                            "mode": "COLOR"
                        },
                        "dataSets": [
                            {
                                "plotType": "LINE",
                                "targetAxis": "Y1",
                                "timeSeriesQuery": {
                                    "timeSeriesFilter": {
                                        "aggregation": {
                                            "perSeriesAligner": "ALIGN_NEXT_OLDER"
                                        },
                                        "filter": "select_slo_budget_fraction(\"${google_monitoring_slo.slo_coverage[each.key].id}\")"
                                    },
                                    "unitOverride": "10^2.%"
                                }
                            }
                        ],
                        "thresholds": [
                            {
                                "targetAxis": "Y1",
                                "value": 1,
                                "label": "100% means ErrBdg not used: Innovation at risk"
                            },
                            {
                                "targetAxis": "Y1",
                                "label": "0% means ErrBdg gone: Reliability at risk"
                            }                        ]
                    }
                }
            },
            {
                "height": 4,
                "width": 12,
                "yPos": 4,
                "widget": {
                    "title": "Error budget: number of bad events remaining vs target over the last ${var.slo_coverage.rolling_period_days} days",
                    "xyChart": {
                        "chartOptions": {
                            "mode": "COLOR"
                        },
                        "dataSets": [
                            {
                                "minAlignmentPeriod": "${tostring(var.slo_coverage.rolling_period_days * 24 * 60 * 60)}s",
                                "plotType": "LINE",
                                "legendTemplate": "Remaining bad events budget",
                                "targetAxis": "Y1",
                                "timeSeriesQuery": {
                                    "timeSeriesFilter": {
                                        "aggregation": {
                                            "alignmentPeriod": "${tostring(var.slo_coverage.rolling_period_days * 24 * 60 * 60)}s"
                                        },
                                        "filter": "select_slo_budget(\"${google_monitoring_slo.slo_coverage[each.key].id}\")"
                                    }
                                }
                            },
                            {
                                "minAlignmentPeriod": "${tostring(var.slo_coverage.rolling_period_days * 24 * 60 * 60)}s",
                                "plotType": "STACKED_AREA",
                                "legendTemplate": "Allowed bad events budget",
                                "targetAxis": "Y1",
                                "timeSeriesQuery": {
                                    "timeSeriesFilter": {
                                        "aggregation": {
                                            "alignmentPeriod": "${tostring(var.slo_coverage.rolling_period_days * 24 * 60 * 60)}s"
                                        },
                                        "filter": "select_slo_budget_total(\"${google_monitoring_slo.slo_coverage[each.key].id}\")"
                                    }
                                }
                            }
                        ],
                        "timeshiftDuration": "0s",
                        "yAxis": {
                            "label": "y1Axis",
                            "scale": "LINEAR"
                        }
                    }
                }
            },
            {
                "height": 4,
                "width": 12,
                "yPos": 8,
                "widget": {
                    "title": "SLO vs SLI - Ratio of good events to good + bad events on the last ${var.slo_coverage.rolling_period_days} days",
                    "xyChart": {
                        "chartOptions": {
                            "mode": "COLOR"
                        },
                        "dataSets": [
                            {
                                "plotType": "LINE",
                                "targetAxis": "Y1",
                                "timeSeriesQuery": {
                                    "timeSeriesFilter": {
                                        "aggregation": {
                                            "perSeriesAligner": "ALIGN_NEXT_OLDER"
                                        },
                                        "filter": "select_slo_compliance(\"${google_monitoring_slo.slo_coverage[each.key].id}\")"
                                    },
                                    "unitOverride": "10^2.%"
                                }
                            }
                        ],
                        "thresholds": [
                            {
                                "targetAxis": "Y1",
                                "value": ${var.slo_coverage.goal}
                            }
                        ]
                    }
                }
            },
            {
                "height": 4,
                "width": 12,
                "yPos": 12,
                "widget": {
                    "title": "SLO vs SLI Ratio of good events to good + bad events on a short window",
                    "xyChart": {
                        "chartOptions": {
                            "mode": "COLOR"
                        },
                        "dataSets": [
                            {
                                "plotType": "LINE",
                                "targetAxis": "Y1",
                                "timeSeriesQuery": {
                                    "timeSeriesFilter": {
                                        "aggregation": {
                                            "perSeriesAligner": "ALIGN_MEAN"
                                        },
                                        "filter": "select_slo_health(\"${google_monitoring_slo.slo_coverage[each.key].id}\")"
                                    },
                                    "unitOverride": "10^2.%"
                                }
                            }
                        ],
                        "thresholds": [
                            {
                                "targetAxis": "Y1",
                                "value": ${var.slo_coverage.goal}
                            }
                        ]
                    }
                }
            },
            {
                "height": 4,
                "width": 12,
                "yPos": 16,
                "widget": {
                    "title": "Count of good and bad events over the last ${var.slo_coverage.rolling_period_days} days",
                    "xyChart": {
                        "chartOptions": {
                            "mode": "COLOR"
                        },
                        "dataSets": [
                            {
                                "minAlignmentPeriod": "${tostring(var.slo_coverage.rolling_period_days * 24 * 60 * 60)}s",
                                "plotType": "LINE",
                                "targetAxis": "Y1",
                                "timeSeriesQuery": {
                                    "timeSeriesFilter": {
                                        "aggregation": {
                                            "alignmentPeriod": "${tostring(var.slo_coverage.rolling_period_days * 24 * 60 * 60)}s",
                                            "perSeriesAligner": "ALIGN_SUM"
                                        },
                                        "filter": "select_slo_counts(\"${google_monitoring_slo.slo_coverage[each.key].id}\")"
                                    }
                                }
                            }
                        ],
                        "timeshiftDuration": "0s",
                        "yAxis": {
                            "label": "y1Axis",
                            "scale": "LINEAR"
                        }
                    }
                }
            }
        ]
    }
}
EOF
}

