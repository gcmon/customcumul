# SLO on custom CUMULATIVE INT64 metric

Key points:

- Monitoring filter syntax:
  - need escape sequences
  - uses one_of function to filter all the goods  
- terraform `for_each` statement enable to create one coverage SLO per table, only one set in this demo
- notification channels for alerting on burnrate have a default pubsub topic to enable the full demo. Can be complemented
- Custom dashboard is needed to see SLO on the 28d aggregation window (rather the meaningless aggregation window of the default SLO graph)

![custom_slo_dashboard_alt](../_docs/custom_slo_dashboard_alt.png)
