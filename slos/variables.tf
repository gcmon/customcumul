/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


variable "project_id" {
  description = "GCP project id"
}

variable "notification_channels" {
  type    = list(string)
  default = []
}

variable "alerting_topic_name" {
  default = "alerting"
}

variable "pubsub_allowed_regions" {
  type = list(string)
  default = [
    "europe-central2",
    "europe-north1",
    "europe-southwest1",
    "europe-west1",
    "europe-west3",
    "europe-west4",
    "europe-west8",
    "europe-west9",
  ]
}

variable "app_name" {
  default = "appA"
}

variable "interval_duration_sec" {
  default = "86400"
}

variable "slo_coverage" {
  description = "coverage slo"
  default = {
    table_list = [
      "tableA",
    ]
    goal                               = 0.96
    rolling_period_days                = 28
    alerting_fast_burn_loopback_period = "1h"
    alerting_fast_burn_threshold       = 10
    alerting_slow_burn_loopback_period = "24h"
    alerting_slow_burn_threshold       = 2
  }
}
