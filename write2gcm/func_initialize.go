// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package write2gcm

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	monitoring "cloud.google.com/go/monitoring/apiv3/v2"
	"cloud.google.com/go/monitoring/apiv3/v2/monitoringpb"
	"github.com/google/uuid"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"google.golang.org/genproto/googleapis/api/label"
	metricpb "google.golang.org/genproto/googleapis/api/metric"
)

const intervalCountMetricType = "custom.googleapis.com/dataproducts/interval/count"
const eventDurationMetricType = "custom.googleapis.com/dataproducts/event/duration"

var global Global
var ctx = context.Background()

// initialize is called by init() and enable tests
func initialize() {
	start := time.Now()
	log.SetFlags(0)
	global.CommonEv.InitID = fmt.Sprintf("%v", uuid.New())
	var err error
	var serviceEnv ServiceEnv

	err = envconfig.Process(microserviceName, &serviceEnv)
	if err != nil {
		log.Println(glo.Entry{
			MicroserviceName: microserviceName,
			Severity:         "CRITICAL",
			Message:          "init_failed",
			Description:      fmt.Sprintf("envconfig.Process %s %v", microserviceName, err),
			InitID:           global.CommonEv.InitID,
		})
		log.Fatalf("INIT_FAILURE %v", err)
	}
	global.serviceEnv = &serviceEnv
	global.CommonEv.MicroserviceName = microserviceName
	global.CommonEv.Environment = global.serviceEnv.Environment
	global.CommonEv.LogOnlySeveritylevels = global.serviceEnv.LogOnlySeveritylevels
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	glo.LogInitColdStart(ev)

	var env Env
	err = envconfig.Process("", &env)
	if err != nil {
		glo.LogInitFatal(ev, "envconfig.Process no prefix", err)
	}
	global.env = &env

	global.metricClient, err = monitoring.NewMetricClient(ctx)
	if err != nil {
		glo.LogInitFatal(ev, "monitoring.NewMetricClient", err)
	}

	metricDescriptors := []*metricpb.MetricDescriptor{
		{
			Type: intervalCountMetricType,
			Labels: []*label.LabelDescriptor{
				{
					Key:         "data_product_name",
					ValueType:   label.LabelDescriptor_STRING,
					Description: "The data product name",
				},
				{
					Key:         "table_name",
					ValueType:   label.LabelDescriptor_STRING,
					Description: "The table product name",
				},
				{
					Key:         "interval_duration_sec",
					ValueType:   label.LabelDescriptor_STRING,
					Description: "example 86400",
				},
				{
					Key:         "good_points",
					ValueType:   label.LabelDescriptor_STRING,
					Description: "one of 0,1,3,4,5,8,9",
				},
			},
			MetricKind:             metricpb.MetricDescriptor_CUMULATIVE,
			ValueType:              metricpb.MetricDescriptor_INT64,
			Unit:                   "1",
			Description:            "For coverage SLO",
			DisplayName:            "Interval count",
			MonitoredResourceTypes: []string{"global"},
		},
		{
			Type: eventDurationMetricType,
			Labels: []*label.LabelDescriptor{
				{
					Key:         "data_product_name",
					ValueType:   label.LabelDescriptor_STRING,
					Description: "The data product name",
				},
				{
					Key:         "table_name",
					ValueType:   label.LabelDescriptor_STRING,
					Description: "The table product name",
				},
			},
			MetricKind:             metricpb.MetricDescriptor_CUMULATIVE,
			ValueType:              metricpb.MetricDescriptor_DISTRIBUTION,
			Unit:                   "s",
			Description:            "For freshness SLO",
			DisplayName:            "Event duration",
			MonitoredResourceTypes: []string{"global"},
		},
	}

	for _, targetMD := range metricDescriptors {
		foundMD, err := getMetricDescriptor(ctx, global.metricClient, global.serviceEnv.ProjectID, targetMD.Type)
		if err != nil {
			if strings.Contains(err.Error(), "NotFound") {
				_, err = createMetricDescriptor(ctx, global.metricClient, global.serviceEnv.ProjectID, targetMD)
				if err != nil {
					glo.LogInitFatal(ev, "createMetricDescriptor", err)
				}
				glo.LogInfo(ev, "MD not found and created", targetMD.Type)
			} else {
				glo.LogInitFatal(ev, "getMetricDescriptor", err)
			}
		} else {
			isEqual, difference := compareMD(targetMD, foundMD)
			if isEqual {
				glo.LogInfo(ev, "MD found no change required", foundMD.Type)
			} else {
				glo.LogInfo(ev, "MD different", fmt.Sprintf("%s %s", targetMD.Type, difference))
				err := deleteMetricDescriptor(ctx, global.metricClient, global.serviceEnv.ProjectID, foundMD.Type)
				if err != nil {
					glo.LogInitFatal(ev, "deleteMetricDescriptor", err)
				}
				glo.LogInfo(ev, "MD found and deleted as requires changes", foundMD.Type)
				_, err = createMetricDescriptor(ctx, global.metricClient, global.serviceEnv.ProjectID, targetMD)
				if err != nil {
					glo.LogInitFatal(ev, "createMetricDescriptor", err)
				}
				glo.LogInfo(ev, "MD re created", targetMD.Type)
			}
		}
	}
	glo.LogInitDone(ev, time.Since(start).Seconds())
}

func getMetricDescriptor(ctx context.Context, metricClient *monitoring.MetricClient, projectID, metricType string) (md *metricpb.MetricDescriptor, err error) {
	req := &monitoringpb.GetMetricDescriptorRequest{
		Name: fmt.Sprintf("projects/%s/metricDescriptors/%s", projectID, metricType),
	}
	md, err = metricClient.GetMetricDescriptor(ctx, req)
	return md, err
}

func createMetricDescriptor(ctx context.Context, metricClient *monitoring.MetricClient, projectID string, targetMD *metricpb.MetricDescriptor) (createdMD *metricpb.MetricDescriptor, err error) {
	req := &monitoringpb.CreateMetricDescriptorRequest{
		Name:             fmt.Sprintf("projects/%s", projectID),
		MetricDescriptor: targetMD,
	}
	createdMD, err = metricClient.CreateMetricDescriptor(ctx, req)
	return createdMD, err
}

func deleteMetricDescriptor(ctx context.Context, metricClient *monitoring.MetricClient, projectID, metricType string) (err error) {
	req := &monitoringpb.DeleteMetricDescriptorRequest{
		Name: fmt.Sprintf("projects/%s/metricDescriptors/%s", projectID, metricType),
	}
	err = metricClient.DeleteMetricDescriptor(ctx, req)
	return err
}

func compareMD(targetMD *metricpb.MetricDescriptor, foundMD *metricpb.MetricDescriptor) (isEqual bool, difference string) {
	if targetMD.MetricKind != foundMD.MetricKind {
		return false, fmt.Sprintf("Kind want %s got %s", targetMD.MetricKind, foundMD.MetricKind)
	}
	if targetMD.ValueType != foundMD.ValueType {
		return false, fmt.Sprintf("Type want %s got %s", targetMD.ValueType, foundMD.ValueType)
	}
	if targetMD.Unit != foundMD.Unit {
		return false, fmt.Sprintf("Unit want %s got %s", targetMD.Unit, foundMD.Unit)
	}
	if targetMD.Description != foundMD.Description {
		return false, fmt.Sprintf("Description want %s got %s", targetMD.Description, foundMD.Description)
	}
	if targetMD.DisplayName != foundMD.DisplayName {
		return false, fmt.Sprintf("DisplayName want %s got %s", targetMD.DisplayName, foundMD.DisplayName)
	}
	if len(targetMD.Labels) != len(foundMD.Labels) {
		return false, fmt.Sprintf("Labels want %d got %d", len(targetMD.Labels), len(foundMD.Labels))
	}
	for _, targetLD := range targetMD.Labels {
		labelEqual := false
		var labelKey string
		for _, foundLD := range foundMD.Labels {
			if targetLD.Key == foundLD.Key {
				if targetLD.ValueType == foundLD.ValueType && targetLD.Description == foundLD.Description {
					labelEqual = true
					break
				}
			}
		}
		if !labelEqual {
			return false, fmt.Sprintf("Label %s not equal", labelKey)
		}
	}
	// The create MD API add much monitored resource types than required, on hold comparison
	// if len(targetMD.MonitoredResourceTypes) != len(foundMD.MonitoredResourceTypes) {
	// 	return false, fmt.Sprintf("Monitored resources want %d got %d", len(targetMD.MonitoredResourceTypes), len(foundMD.MonitoredResourceTypes))
	// }
	// for _, targetMRT := range targetMD.MonitoredResourceTypes {
	// 	resourceEqual := false
	// 	for _, foundMRT := range foundMD.MonitoredResourceTypes {
	// 		if targetMRT == foundMRT {
	// 			resourceEqual = true
	// 			break
	// 		}
	// 	}
	// 	if !resourceEqual {
	// 		return false, fmt.Sprintf("Monitored resource %s not found", targetMRT)
	// 	}
	// }
	return true, ""
}
