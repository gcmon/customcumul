// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package write2gcm

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func EntryPoint(ctxEvent context.Context, pubsubMsg pubsub.Message) error {
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv

	now := time.Now()
	d := now.Sub(pubsubMsg.PublishTime)

	b, err := json.MarshalIndent(pubsubMsg, "", "  ")
	if err != nil {
		glo.LogWarning(ev, "unable to marshall indent pubsub message", err.Error())
	} else {
		glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)
	}

	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("scheduler/pubsub/%s", fmt.Sprintf("%s%v", pubsubMsg.Data, pubsubMsg.PublishTime)),
		StepTimestamp: pubsubMsg.PublishTime,
	}
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, ev.Step)

	switch string(pubsubMsg.Data) {
	case "at_every_minute":
		err := writeTimeseriesIntervalCount()
		if err != nil {
			glo.LogCriticalNoRetry(ev, fmt.Sprintf("writeTimeseries %v", err), "", "")
		}
	default:
		glo.LogWarning(ev, "unmanaged scheduler pubsub message", string(pubsubMsg.Data))
	}

	glo.LogFinish(ev, "done", string(pubsubMsg.Data), time.Now(), "", "", "", "", 0)

	return nil
}
