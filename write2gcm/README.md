# write2gcm

Key points:

- Every minute
- Write two times series to a CUMULATIVE INT6$ metric
  - timestamps
    - End  is now
    - Start is now minus 59 sec
  - Value factor is a random integer between 0 and 99
  - the bad timeseries has
    - a `good_points` label value random from "0" or "1" or "3" or "4" score
    - a value of 2x value factor
  - the good timeseries has
    - a `good_points` label value random from "5" or "8" or "9" score
    - a value of 98x value factor
  - Metric descriptors are created when not exist, deleted and recreate when change is needed, during initialization

Example of resulting timeseries listed using the API [`Method: projects.timeSeries.list`](https://cloud.google.com/monitoring/api/ref_v3/rest/v3/projects.timeSeries/list)

```json
{
  "timeSeries": [
    {
      "metric": {
        "labels": {
          "interval_duration_sec": "86400",
          "good_points": "0",
          "table_name": "tableA",
          "data_product_name": "appA"
        },
        "type": "custom.googleapis.com/dataproducts/interval/count"
      },
      "resource": {
        "type": "global",
        "labels": {
          "project_id": "redacted"
        }
      },
      "metricKind": "CUMULATIVE",
      "valueType": "INT64",
      "points": [
        {
          "interval": {
            "startTime": "2024-03-08T13:26:01Z",
            "endTime": "2024-03-08T13:27:00Z"
          },
          "value": {
            "int64Value": "90"
          }
        }
      ]
    },
    {
      "metric": {
        "labels": {
          "data_product_name": "appA",
          "interval_duration_sec": "86400",
          "table_name": "tableA",
          "good_points": "1"
        },
        "type": "custom.googleapis.com/dataproducts/interval/count"
      },
      "resource": {
        "type": "global",
        "labels": {
          "project_id": "redacted"
        }
      },
      "metricKind": "CUMULATIVE",
      "valueType": "INT64",
      "points": [
        {
          "interval": {
            "startTime": "2024-03-08T13:27:01Z",
            "endTime": "2024-03-08T13:28:00Z"
          },
          "value": {
            "int64Value": "146"
          }
        }
      ]
    },
    {
      "metric": {
        "labels": {
          "good_points": "3",
          "table_name": "tableA",
          "interval_duration_sec": "86400",
          "data_product_name": "appA"
        },
        "type": "custom.googleapis.com/dataproducts/interval/count"
      },
      "resource": {
        "type": "global",
        "labels": {
          "project_id": "redacted"
        }
      },
      "metricKind": "CUMULATIVE",
      "valueType": "INT64",
      "points": [
        {
          "interval": {
            "startTime": "2024-03-08T13:29:01Z",
            "endTime": "2024-03-08T13:30:00Z"
          },
          "value": {
            "int64Value": "46"
          }
        },
        {
          "interval": {
            "startTime": "2024-03-08T13:28:01Z",
            "endTime": "2024-03-08T13:29:00Z"
          },
          "value": {
            "int64Value": "176"
          }
        }
      ]
    },
    {
      "metric": {
        "labels": {
          "data_product_name": "appA",
          "table_name": "tableA",
          "interval_duration_sec": "86400",
          "good_points": "8"
        },
        "type": "custom.googleapis.com/dataproducts/interval/count"
      },
      "resource": {
        "type": "global",
        "labels": {
          "project_id": "redacted"
        }
      },
      "metricKind": "CUMULATIVE",
      "valueType": "INT64",
      "points": [
        {
          "interval": {
            "startTime": "2024-03-08T13:29:01Z",
            "endTime": "2024-03-08T13:30:00Z"
          },
          "value": {
            "int64Value": "2254"
          }
        },
        {
          "interval": {
            "startTime": "2024-03-08T13:28:01Z",
            "endTime": "2024-03-08T13:29:00Z"
          },
          "value": {
            "int64Value": "8624"
          }
        }
      ]
    },
    {
      "metric": {
        "labels": {
          "data_product_name": "appA",
          "table_name": "tableA",
          "interval_duration_sec": "86400",
          "good_points": "9"
        },
        "type": "custom.googleapis.com/dataproducts/interval/count"
      },
      "resource": {
        "type": "global",
        "labels": {
          "project_id": "redacted"
        }
      },
      "metricKind": "CUMULATIVE",
      "valueType": "INT64",
      "points": [
        {
          "interval": {
            "startTime": "2024-03-08T13:27:01Z",
            "endTime": "2024-03-08T13:28:00Z"
          },
          "value": {
            "int64Value": "7154"
          }
        },
        {
          "interval": {
            "startTime": "2024-03-08T13:26:01Z",
            "endTime": "2024-03-08T13:27:00Z"
          },
          "value": {
            "int64Value": "4410"
          }
        }
      ]
    }
  ],
  "unit": "1"
}
```
