
/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


variable "project_id" {
  description = "GCP project id"
  type        = string
}

variable "container_images_registry" {
  description = "artifact registry path"
  type        = string
}

variable "action_trigger_topic_name" {
  description = "the message body is the key used to fetch which actions to trigger"
  default     = "actionTrigger"
  type        = string
}

variable "pubsub_allowed_regions" {
  type = list(string)
  default = [
    "europe-central2",
    "europe-north1",
    "europe-southwest1",
    "europe-west1",
    "europe-west3",
    "europe-west4",
    "europe-west8",
    "europe-west9",
  ]
}

variable "schedulers" {
  type = map(any)
  default = {
    dev_every_min = {
      "environment" = "dev",
      "name"        = "at_every_minute",
      "schedule"    = "* * * * *",
    },
  }
}

variable "scheduler_region" {
  description = "Cloud Scheduler region"
  type        = string
  default     = "europe-west1"
}

variable "crun_region" {
  description = "cloud run region"
  type        = string
  default     = "europe-west1"
}

variable "crun_cpu" {
  description = "Number of cpu in k8s quantity 1000m means 1000 milli cpu aka 1"
  type        = string
  default     = "1000m"
}

variable "crun_memory" {
  description = "Memory allocation in k8s quantity "
  type        = string
  default     = "128Mi"
}

variable "crun_concurrency" {
  description = "Number of requests a container could received at the same time"
  type        = number
  default     = 80
}

variable "crun_max_instances" {
  description = "Max number of instances"
  type        = number
  default     = 1000
}

variable "crun_timeout" {
  description = "Max duration for an instance for responding to a request"
  type        = string
  default     = "180s"
}

variable "microservice_image_digest" {
  description = "The container image signature for this microservice like sha256:xxxxxxx"
  type        = string
}

variable "environment" {
  description = "environment name"
  type        = string
  default     = "dev"
}

variable "log_only_severity_levels" {
  description = "Which type of log entry should be logged"
  type        = string
  default     = "WARNING NOTICE CRITICAL INFO"
}

variable "sub_ack_deadline_seconds" {
  description = "The maximum time after a subscriber receives a message before the subscriber should acknowledge the message"
  type        = number
  default     = 260
}

variable "sub_message_retention_duration" {
  description = "How long to retain unacknowledged messages in the subscription's backlog,"
  type        = string
  default     = "86400s"
}

variable "sub_minimum_backoff" {
  description = "The minimum delay between consecutive deliveries of a given message"
  type        = string
  default     = "10s"
}
