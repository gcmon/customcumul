// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package write2gcm

import (
	"fmt"
	"math/rand"
	"time"

	"cloud.google.com/go/monitoring/apiv3/v2/monitoringpb"
	"github.com/golang/protobuf/ptypes/timestamp"
	metricpb "google.golang.org/genproto/googleapis/api/metric"
	monitoredres "google.golang.org/genproto/googleapis/api/monitoredres"
)

func writeTimeseriesIntervalCount() error {
	var baseBad, baseGood, factor int64
	baseBad = 2
	baseGood = 98
	factor = rand.Int63n(100)
	countBad := baseBad * factor
	countGood := baseGood * factor

	nowUXT := time.Now().Unix()
	endTime := &timestamp.Timestamp{
		Seconds: nowUXT,
	}
	startTime := &timestamp.Timestamp{
		Seconds: nowUXT - 59,
	}

	badLabels := []string{"0", "1", "3", "4"}
	metricBad := &metricpb.Metric{
		Type: intervalCountMetricType,
		Labels: map[string]string{
			"data_product_name":     "appA",
			"table_name":            "tableA",
			"interval_duration_sec": "86400",
			"good_points":           badLabels[rand.Intn(len(badLabels))],
		},
	}

	goodLabels := []string{"5", "8", "9"}
	metricGood := &metricpb.Metric{
		Type: intervalCountMetricType,
		Labels: map[string]string{
			"data_product_name":     "appA",
			"table_name":            "tableA",
			"interval_duration_sec": "86400",
			"good_points":           goodLabels[rand.Intn(len(goodLabels))],
		},
	}

	pointsBad := []*monitoringpb.Point{{
		Interval: &monitoringpb.TimeInterval{
			StartTime: startTime,
			EndTime:   endTime,
		},
		Value: &monitoringpb.TypedValue{
			Value: &monitoringpb.TypedValue_Int64Value{
				Int64Value: countBad,
			},
		},
	}}

	pointsGood := []*monitoringpb.Point{{
		Interval: &monitoringpb.TimeInterval{
			StartTime: startTime,
			EndTime:   endTime,
		},
		Value: &monitoringpb.TypedValue{
			Value: &monitoringpb.TypedValue_Int64Value{
				Int64Value: countGood,
			},
		},
	}}

	var tSeriesBad, tSeriesGood monitoringpb.TimeSeries

	tSeriesBad.Metric = metricBad
	tSeriesBad.Points = pointsBad
	tSeriesBad.Resource = &monitoredres.MonitoredResource{
		Type: "global",
	}

	tSeriesGood.Metric = metricGood
	tSeriesGood.Points = pointsGood
	tSeriesGood.Resource = &monitoredres.MonitoredResource{
		Type: "global",
	}

	var tSeriesCol []*monitoringpb.TimeSeries
	tSeriesCol = append(tSeriesCol, &tSeriesBad)
	tSeriesCol = append(tSeriesCol, &tSeriesGood)

	var req monitoringpb.CreateTimeSeriesRequest
	req.Name = fmt.Sprintf("projects/%s", global.serviceEnv.ProjectID)
	req.TimeSeries = tSeriesCol

	return global.metricClient.CreateTimeSeries(ctx, &req)
}
