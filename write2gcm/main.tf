
/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  environment  = "dev"
  service_name = "write2gcm"
}

data "google_project" "project" {
  project_id = var.project_id
}

resource "google_artifact_registry_repository" "gcmon-repo" {
  provider      = google-beta
  project       = var.project_id
  location      = "europe"
  repository_id = "gcmon"
  description   = "Docker images for gcmon tools"
  format        = "DOCKER"
}

resource "google_artifact_registry_repository_iam_member" "reader_public" {
  provider   = google-beta
  project    = google_artifact_registry_repository.gcmon-repo.project
  location   = google_artifact_registry_repository.gcmon-repo.location
  repository = google_artifact_registry_repository.gcmon-repo.name
  role       = "roles/artifactregistry.reader"
  member     = "allUsers"
}

resource "google_service_account" "microservice_sa" {
  project      = var.project_id
  account_id   = local.service_name
  display_name = "gcmom ${local.service_name}"
  description  = "Solution: gcmom, microservice: ${local.service_name}"
}

resource "google_project_iam_member" "project_monitoring_editor" {
  project = var.project_id
  role    = "roles/monitoring.editor"
  member  = "serviceAccount:${google_service_account.microservice_sa.email}"
}

resource "google_pubsub_topic" "action_trigger" {
  project = var.project_id
  name    = var.action_trigger_topic_name
  message_storage_policy {
    allowed_persistence_regions = var.pubsub_allowed_regions
  }
}

resource "google_cloud_scheduler_job" "job" {
  for_each = {
    for name, s in var.schedulers : name => s
    if s.environment == local.environment
  }
  project     = var.project_id
  name        = each.value.name
  description = "gcmon ${each.value.name}"
  schedule    = each.value.schedule
  region      = var.scheduler_region

  pubsub_target {
    topic_name = google_pubsub_topic.action_trigger.id
    data       = base64encode(each.value.name)
  }
}

resource "google_cloud_run_v2_service" "crun_svc" {
  project  = var.project_id
  name     = local.service_name
  location = var.crun_region
  # deletion_protection = false
  client  = "terraform"
  ingress = "INGRESS_TRAFFIC_INTERNAL_ONLY"
  binary_authorization {
    use_default = true
  }
  traffic {
    type    = "TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST"
    percent = 100
  }
  template {
    execution_environment = "EXECUTION_ENVIRONMENT_GEN1"
    containers {
      image = "${var.container_images_registry}/${local.service_name}@${var.microservice_image_digest}"
      resources {
        cpu_idle = true
        limits = {
          cpu    = "${var.crun_cpu}"
          memory = "${var.crun_memory}"
        }
      }
      env {
        name  = "${upper(local.service_name)}_ENVIRONMENT"
        value = var.environment
      }
      env {
        name  = "${upper(local.service_name)}_LOG_ONLY_SEVERITY_LEVELS"
        value = var.log_only_severity_levels
      }
      env {
        name  = "${upper(local.service_name)}_PROJECT_ID"
        value = var.project_id
      }
    }
    max_instance_request_concurrency = var.crun_concurrency
    timeout                          = var.crun_timeout
    service_account                  = google_service_account.microservice_sa.email
    scaling {
      max_instance_count = var.crun_max_instances
    }
  }
}

resource "google_service_account" "subscription_sa" {
  project      = var.project_id
  account_id   = "trigger-${local.service_name}"
  display_name = "Execute ${local.service_name} trigger"
  description  = "Solution: gcmon, microservice trigger: ${local.service_name}"
}
data "google_iam_policy" "binding" {
  binding {
    role = "roles/run.invoker"
    members = [
      "serviceAccount:${google_service_account.subscription_sa.email}",
    ]
  }
}
resource "google_cloud_run_service_iam_policy" "trigger_invoker" {
  location = google_cloud_run_v2_service.crun_svc.location
  project  = google_cloud_run_v2_service.crun_svc.project
  service  = google_cloud_run_v2_service.crun_svc.name

  policy_data = data.google_iam_policy.binding.policy_data
}

resource "google_pubsub_subscription" "subscription" {
  project              = var.project_id
  name                 = local.service_name
  topic                = google_pubsub_topic.action_trigger.id
  ack_deadline_seconds = var.sub_ack_deadline_seconds
  push_config {
    oidc_token {
      service_account_email = google_service_account.subscription_sa.email
    }
    #Updated endpoint to deal with WARNING in logs: failed to extract Pub/Sub topic name from the URL request path: "/", configure your subscription's push endpoint to use the following path pattern: 'projects/PROJECT_NAME/topics/TOPIC_NAME
    push_endpoint = "${google_cloud_run_v2_service.crun_svc.uri}/${google_pubsub_topic.action_trigger.id} "
  }
  expiration_policy {
    ttl = ""
  }
  message_retention_duration = var.sub_message_retention_duration
  retry_policy {
    minimum_backoff = var.sub_minimum_backoff
  }
}

resource "google_binary_authorization_policy" "policy" {
  project = var.project_id
  default_admission_rule {
    evaluation_mode = "REQUIRE_ATTESTATION"
    enforcement_mode = "ENFORCED_BLOCK_AND_AUDIT_LOG"
    require_attestations_by = [
      "projects/${var.project_id}/attestors/built-by-cloud-build",
    ]
  }
}
