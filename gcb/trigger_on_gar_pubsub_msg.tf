/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

resource "google_pubsub_topic" "gcr" {
  project = var.project_id
  name    = "gcr"
}

resource "google_cloudbuild_trigger" "on_gar_pubsub_msg" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.project_id
  location        = var.region
  name            = "onGARPubSubMsg"
  description     = "On Google Artifact Registry PubSub message do something"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = ["gar", "pubsub"]
  pubsub_config {
    topic = google_pubsub_topic.gcr.id
  }
  substitutions = {
    _ACTION = "$(body.message.data.action)"
    _DIGEST = "$(body.message.data.digest)"
    _TAG = "$(body.message.data.tag)"
  }
  filter = "_TAG != \"\" && _ACTION == \"INSERT\""
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging = "GCS_ONLY"
    }
    step {
      name       = "alpine"
      id         = "Show Pubsub message data"
      entrypoint = "sh"
      args = ["-c", <<-EOF
        echo "action: $_ACTION"
        echo "tag:    $_TAG"
        echo "digest: $_DIGEST"
EOF
      ]
    }
  }
}
