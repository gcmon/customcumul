/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

resource "google_cloudbuild_trigger" "on_pr_2_trunk_write2gcm" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.project_id
  location        = var.region
  name            = "onPR2Trunk-write2gcm"
  description     = "When push to a feature branch with an open Merge/Pull Request to the trunk branch, includes source files of this microservice"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = ["write2gcm", "build"]
  repository_event_config {
    pull_request {
      branch          = "^main$"
      invert_regex    = false
      comment_control = "COMMENTS_ENABLED_FOR_EXTERNAL_CONTRIBUTORS_ONLY"
    }
    repository = "projects/${var.project_id}/locations/${var.region}/connections/GitLab/repositories/${var.repo_name}"
  }
  included_files = ["write2gcm/**/*.go", "write2gcm/go.mod", "write2gcm/go.sum"]
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging                 = "GCS_ONLY"
      requested_verify_option = "VERIFIED"
    }
    artifacts {
      objects {
        location = "gs://${google_storage_bucket.cloud_build_artifacts.name}/write2gcm/"
        paths    = ["write2gcm/*_coverage_*.html"]
      }
    }
    step {
      name       = "golang:${var.golang_version}"
      id         = "go tests + go vulnerability check"
      dir        = "write2gcm"
      entrypoint = "sh"
      env = [
        "PROJECT_ID=${var.project_id}",
        "BUILD_ID=$BUILD_ID",
      ]
      args = ["-c", <<-EOF
    set -e
    go version
    go test ./... -coverprofile=coverage.out
    go tool cover -html=coverage.out -o coverage.html
    go tool cover -func=coverage.out
    mv coverage.html "$(date +%Y-%m-%d_%H:%M)_coverage_$BUILD_ID.html"
    go install golang.org/x/vuln/cmd/govulncheck@latest
    govulncheck ./...
  EOF
      ]
    }
    step {
      name = "gcr.io/k8s-skaffold/pack"
      id   = "build"
      dir  = "write2gcm"
      args = [
        "pack",
        "build",
        "${var.artifact_registry}/${var.project_id}/${var.artifact_repo_name}/write2gcm",
        "--builder",
        "${var.cnb_builder_image}",
        "--env",
        "GOOGLE_FUNCTION_SIGNATURE_TYPE=event",
        "--env",
        "GOOGLE_FUNCTION_TARGET=EntryPoint",
      ]
    }
    images = ["${var.artifact_registry}/${var.project_id}/${var.artifact_repo_name}/write2gcm"]
  }
}
