/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

resource "google_cloudbuild_trigger" "on_cao_built_by_cloud_build_pubsub_msg" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.project_id
  location        = var.region
  name            = "onCAOBuiltByCloudBuiltPubSubMsg"
  description     = "On Container Analysis Occurrence ATTESTATION with note indicating built-by-cloud-build PubSub message get occurrence details"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = ["cao","attestation","built-by-cloud-build", "pubsub"]
  pubsub_config {
    topic = "projects/${var.project_id}/topics/container-analysis-occurrences-v1"
  }
  substitutions = {
    _JSON_DATA = "$(body.message.data)"
    _KIND       = "$(body.message.data.kind)"
  # _OCCURRENCE = "$(body.message.data.name)"
    _TIME       = "$(body.message.data.notificationTime)"
  }
  filter = "_KIND == \"ATTESTATION\""
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging = "GCS_ONLY"
    }
    step {
      name       = "alpine"
      id         = "Get occurrence reference"
      entrypoint = "sh"
      args = ["-c", <<-EOF
        apk add sed
        echo data: $_JSON_DATA
        echo "data is missing double quote for valid JSON format"
        echo $_JSON_DATA | sed -E 's|.*name:(.*),notificationTime:.*|\1|' | tee /workspace/occurrence.txt
EOF
      ]
    }
    step {
      name       = "google/cloud-sdk:slim" # need gcloud auth
      id         = "Export SBOM when attest is built-by-cloud-run"
      entrypoint = "sh"
      args = ["-c", <<-EOF
        apt-get update && apt-get install -y jq
        echo kind:             $_KIND
        echo notificationTime: $_TIME
        curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $(gcloud auth print-access-token)" https://containeranalysis.googleapis.com/v1/$(cat /workspace/occurrence.txt) | tee /workspace/attestation.json
        if cat /workspace/attestation.json | jq -r '.noteName | contains("built-by-cloud-build")'; then
          gcloud artifacts sbom export --uri=$(cat /workspace/attestation.json | jq -r '.resourceUri')
          echo "SBOM exported to GCS"
        else
          echo "Skipping SBOM export as noteName does not contain 'built-by-cloud-build'"
        fi
EOF
      ]
    }
  }
}

# resource "google_cloudbuild_trigger" "on_cao_vulnerability_pubsub_msg" {
#   depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
#   project         = var.project_id
#   location        = var.region
#   name            = "onCAOVulnerabilityPubSubMsg"
#   description     = "On Container Analysis Occurrence VULNERABILITY PubSub message get occurrence details"
#   service_account = google_service_account.cbuild_custom_sa.id
#   tags            = ["cao","vulnerability", "pubsub"]
#   pubsub_config {
#     topic = "projects/${var.project_id}/topics/container-analysis-occurrences-v1"
#   }
#   substitutions = {
#     _JSON_DATA = "$(body.message.data)"
#     _KIND       = "$(body.message.data.kind)"
#   # _OCCURRENCE = "$(body.message.data.name)"
#     _TIME       = "$(body.message.data.notificationTime)"
#   }
#   filter = "_KIND == \"VULNERABILITY\""
#   build {
#     logs_bucket = google_storage_bucket.cloud_build_logs.url
#     options {
#       logging = "GCS_ONLY"
#     }
#     step {
#       name       = "alpine"
#       id         = "Get occurrence reference"
#       entrypoint = "sh"
#       args = ["-c", <<-EOF
#         apk add sed
#         echo data: $_JSON_DATA
#         echo "data is missing double quote for valid JSON format"
#         echo $_JSON_DATA | sed -E 's|.*name:(.*),notificationTime:.*|\1|' | tee /workspace/occurrence.txt
# EOF
#       ]
#     }
#     step {
#       name       = "google/cloud-sdk:slim" # need gcloud auth
#       id         = "Show occurrence details"
#       entrypoint = "sh"
#       args = ["-c", <<-EOF
#         echo kind:             $_KIND
#         echo notificationTime: $_TIME
#         curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $(gcloud auth print-access-token)" https://containeranalysis.googleapis.com/v1/$(cat /workspace/occurrence.txt)
# EOF
#       ]
#     }
#   }
# }

# resource "google_cloudbuild_trigger" "on_cao_attestation_pubsub_msg" {
#   depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
#   project         = var.project_id
#   location        = var.region
#   name            = "onCAOAttestationPubSubMsg"
#   description     = "On Container Analysis Occurrence ATTESTATION PubSub message get occurrence details"
#   service_account = google_service_account.cbuild_custom_sa.id
#   tags            = ["cao","attestation", "pubsub"]
#   pubsub_config {
#     topic = "projects/${var.project_id}/topics/container-analysis-occurrences-v1"
#   }
#   substitutions = {
#     _JSON_DATA = "$(body.message.data)"
#     _KIND       = "$(body.message.data.kind)"
#   # _OCCURRENCE = "$(body.message.data.name)"
#     _TIME       = "$(body.message.data.notificationTime)"
#   }
#   filter = "_KIND == \"ATTESTATION\""
#   build {
#     logs_bucket = google_storage_bucket.cloud_build_logs.url
#     options {
#       logging = "GCS_ONLY"
#     }
#     step {
#       name       = "alpine"
#       id         = "Get occurrence reference"
#       entrypoint = "sh"
#       args = ["-c", <<-EOF
#         apk add sed
#         echo data: $_JSON_DATA
#         echo "data is missing double quote for valid JSON format"
#         echo $_JSON_DATA | sed -E 's|.*name:(.*),notificationTime:.*|\1|' | tee /workspace/occurrence.txt
# EOF
#       ]
#     }
#     step {
#       name       = "google/cloud-sdk:slim" # need gcloud auth
#       id         = "Show occurrence details"
#       entrypoint = "sh"
#       args = ["-c", <<-EOF
#         echo kind:             $_KIND
#         echo notificationTime: $_TIME
#         curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $(gcloud auth print-access-token)" https://containeranalysis.googleapis.com/v1/$(cat /workspace/occurrence.txt)
# EOF
#       ]
#     }
#   }
# }

# resource "google_cloudbuild_trigger" "on_cao_build_pubsub_msg" {
#   depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
#   project         = var.project_id
#   location        = var.region
#   name            = "onCAOBuildPubSubMsg"
#   description     = "On Container Analysis Occurrence BUILD PubSub message get occurrence details"
#   service_account = google_service_account.cbuild_custom_sa.id
#   tags            = ["cao","build", "pubsub"]
#   pubsub_config {
#     topic = "projects/${var.project_id}/topics/container-analysis-occurrences-v1"
#   }
#   substitutions = {
#     _JSON_DATA = "$(body.message.data)"
#     _KIND       = "$(body.message.data.kind)"
#   # _OCCURRENCE = "$(body.message.data.name)"
#     _TIME       = "$(body.message.data.notificationTime)"
#   }
#   filter = "_KIND == \"BUILD\""
#   build {
#     logs_bucket = google_storage_bucket.cloud_build_logs.url
#     options {
#       logging = "GCS_ONLY"
#     }
#     step {
#       name       = "alpine"
#       id         = "Get occurrence reference"
#       entrypoint = "sh"
#       args = ["-c", <<-EOF
#         apk add sed
#         echo data: $_JSON_DATA
#         echo "data is missing double quote for valid JSON format"
#         echo $_JSON_DATA | sed -E 's|.*name:(.*),notificationTime:.*|\1|' | tee /workspace/occurrence.txt
# EOF
#       ]
#     }
#     step {
#       name       = "google/cloud-sdk:slim" # need gcloud auth
#       id         = "Show occurrence details"
#       entrypoint = "sh"
#       args = ["-c", <<-EOF
#         echo kind:             $_KIND
#         echo notificationTime: $_TIME
#         curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $(gcloud auth print-access-token)" https://containeranalysis.googleapis.com/v1/$(cat /workspace/occurrence.txt)
# EOF
#       ]
#     }
#   }
# }
