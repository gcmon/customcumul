/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "GCP project id"
  type        = string
}

variable "region" {
  description = "gcp region"
  type        = string
  default     = "europe-west1"
}

variable "repo_name" {
  description = "Gitlab repo name"
  type = string
}

variable "artifact_registry" {
  description = "Which google artifact registry: europe, us, asia"
  type = string
  default = "europe-docker.pkg.dev"
}

variable "artifact_repo_name" {
  description = "Docker image repo name"
  type = string
  default = "gcmon"
}

variable "cnb_builder_image" {
  description = "Google builder URL"
  type = string
  default = "gcr.io/buildpacks/builder:google-22"
}

variable "golang_version" {
  description = "The version of Golang used"
  type = string
  default = "1.24.0"
}