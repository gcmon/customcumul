# Cloud Build triggers and pipelines

## How `gcr` topic messages look like?

```shell
gcloud pubsub subscriptions create gcr-sub --topic gcr --project=$PROJECT_ID
gcloud pubsub subscriptions pull gcr-sub --project=$PROJECT_ID --auto-ack --limit=1 --format="json(message.data)" | jq .
```

The Pubsub data string is Base64 encoded. Once decoded look like this:

When deleting an image:

```json
{
  "action": "DELETE",
  "digest": "europe-docker.pkg.dev/<your-project-id>/gcmon/write2gcm@sha256:c053d2e1b243a2fd7302333c9dac9e6b3902eca1bf1641ed7a7ddacc61da5eb2"
}
```

When publishing a new image:

```json
{
  "action": "INSERT",
  "digest": "europe-docker.pkg.dev/<your-project-id>/gcmon/write2gcm@sha256:8c3dcbf83e5919b5522b4321cf7549684d9dc838cc3099d11d81be7256440eba",
  "tag": "europe-docker.pkg.dev/<your-project-id>/gcmon/write2gcm:latest"
}
```

When adding a tag `v0.0.1`:

```json
{
  "action": "INSERT",
  "digest": "europe-docker.pkg.dev/<your-project-id>/gcmon/write2gcm@sha256:8c3dcbf83e5919b5522b4321cf7549684d9dc838cc3099d11d81be7256440eba",
  "tag": "europe-docker.pkg.dev/<your-project-id>/gcmon/write2gcm:v0.0.1"
}
```

When removing a tag `v0.0.1`:

```json
{
  "action": "DELETE",
  "tag": "europe-docker.pkg.dev/<your-project-id>/gcmon/write2gcm:v0.0.1"
}
```

## How `container-analysis-notes-v1` topic messages look like?

```shell
gcloud pubsub subscriptions create can-sub --topic container-analysis-notes-v1 --project=$PROJECT_ID
gcloud pubsub subscriptions pull can-sub --project=$PROJECT_ID --auto-ack --limit=1 --format="json(message.data)" | jq .
```

The Pubsub data string is Base64 encoded. Once decoded look like this:

## How `container-analysis-occurrences-v1` topic messages look like?

```shell
gcloud pubsub subscriptions create cao-sub --topic container-analysis-occurrences-v1 --project=$PROJECT_ID
gcloud pubsub subscriptions pull cao-sub --project=$PROJECT_ID --auto-ack --limit=1 --format="json(message.data)" | jq .
```

The Pubsub data string is Base64 encoded. Once decoded look like this:

```json
{
  "name": "projects/<your-project-id>/locations/eu/occurrences/901e3e4c-c508-419c-aa35-157316d614bc",
  "kind": "PACKAGE",
  "notificationTime": "2025-01-15T07:27:58.421784Z"
}
```
