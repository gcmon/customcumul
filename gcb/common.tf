/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

resource "google_service_account" "cbuild_custom_sa" {
  project      = var.project_id
  account_id   = "gcb-gcm"
  display_name = "Cloud Build for Cloud Monitoring"
  description  = "Used to run Cloud Monitoring related pipelines"
}

resource "google_storage_bucket" "cloud_build_logs" {
  project  = var.project_id
  name     = "${var.project_id}-cloud-build-logs"
  location = var.region
}

resource "google_storage_bucket" "cloud_build_artifacts" {
  project  = var.project_id
  name     = "${var.project_id}-cloud-build-artifacts"
  location = var.region
  lifecycle_rule {
    condition {
      age = 30 # days
    }
    action {
      type = "Delete"
    }
  }
}

resource "google_project_iam_member" "custom_cloud_build_sa_storage_adm" {
  project = var.project_id
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}


resource "google_project_iam_member" "custom_cloud_build_sa_srv_agt" {
  project = var.project_id
  role    = "roles/cloudbuild.serviceAgent"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}



