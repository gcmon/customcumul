# Explore custom metrics based SLOs

## Write custom metrics

`write2gcm` folder host code sample to:

- Build a containerized go app that create metric descriptor and writes custom time series
  - `custom.googleapis.com/dataproducts/interval/count`
    - kind CUMULATIVE
    - type INT64
- Deploy cloud resources using Terraform to schedule the above app execution in Cloud Run

## Implement SLOs

`slos` folder host code sample to provision:

- A Cloud Monitoring Coverage SLO based on the `interval/count` metric
- A slow burn rate alert for this SLO
- A fast burn rate alert for this SLO
- A custom SLO dashboard to complement the default one

![custom_slo_dashboard](_docs/custom_slo_dashboard.png)
